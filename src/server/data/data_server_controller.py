from typing import List, Union
import uuid as uuid_lib
from common import LocalGame, Player
from common.data_structures.profiles import Profile

from server.data.comm_server_calls_data_impl import CommServerCallsDataImpl
from common.interfaces.i_comm_server_calls_data import I_CommServerCallsData


class DataServerController:
    def __init__(self):
        self.my_interface_from_comm_server = CommServerCallsDataImpl(self)

        self.game_list: List[LocalGame] = []
        self.player_list: List[Profile] = []

    def get_my_interface_from_comm_server(self) -> I_CommServerCallsData:
        return self.my_interface_from_comm_server

    def find_game_with_uuid(self, game_uuid: uuid_lib.UUID) -> Union[LocalGame, None]:
        for game in self.game_list:
            if game.game_uuid == game_uuid:
                return game
        return None

    def find_player_with_uuid(self, player_id: uuid_lib.UUID) -> Union[Profile, None]:
        for player in self.player_list:
            if player.uuid == player_id:
                return player
        return None
