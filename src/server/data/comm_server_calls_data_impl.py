from __future__ import annotations

from typing import TYPE_CHECKING, Optional

if TYPE_CHECKING:
    from server.data.data_server_controller import DataServerController
from typing import List, Tuple
import uuid as uuid_lib
from common import (
    LocalGame,
    PublicGame,
    Player,
    Profile,
    Move,
    Message,
    I_CommServerCallsData,
)


class CommServerCallsDataImpl(I_CommServerCallsData):
    def __init__(self, data_server_controller: DataServerController):
        self.data_server_controller = data_server_controller

    def create_game(self, local_game: LocalGame) -> PublicGame:
        # self.data_server_controller.game_list.append(local_game)
        pass

    def get_players(self) -> List:
        return self.data_server_controller.player_list

    def get_available_games(self) -> List[PublicGame]:
        available_games_list: List[PublicGame] = []
        for games in self.data_server_controller.game_list:
            if games.status == "available":
                available_games_list.append(games.convert_to_public_game())
        return available_games_list

    def add_user(self, profile: Profile) -> Player:
        new_player = Player(profile.nickname, profile.uuid)
        if new_player not in self.data_server_controller.player_list:
            self.data_server_controller.player_list.append(profile)
        else:
            pass  # See how to handle errors
        return new_player

    def add_spectator_to_game(
        self, user_id: uuid_lib.UUID, game_id: uuid_lib.UUID
    ) -> LocalGame:
        pass

    def delete_spectator_from_game(
        self, user_id: uuid_lib.UUID, game_id: uuid_lib.UUID
    ) -> None:
        pass

    def get_game_spectators(self, game_id: uuid_lib.UUID) -> List[Player]:
        game = self.data_server_controller.find_game_with_uuid(game_id)
        if game:
            return game.spectators
        else:
            return []

    def get_game_players(self, game_id: uuid_lib.UUID) -> List[Profile]:
        game = self.data_server_controller.find_game_with_uuid(game_id)
        listPlayer: List[Profile] = []
        if game:
            if game.white_player:
                listPlayer.append(game.white_player)
            if game.red_player:
                listPlayer.append(game.red_player)
        return listPlayer

    def delete_game(self, game_id: uuid_lib.UUID) -> None:
        game = self.data_server_controller.find_game_with_uuid(game_id)
        if game:
            self.data_server_controller.game_list.remove(game)

    def add_player_to_game(
        self, game_id: uuid_lib.UUID, user_id: uuid_lib.UUID
    ) -> LocalGame:
        pass

    def get_profile(self, user_id: uuid_lib.UUID) -> Profile:
        pass

    def add_move(self, move: Move) -> None:
        pass

    def is_game_finished(self, game_id: uuid_lib.UUID) -> bool:
        game = self.data_server_controller.find_game_with_uuid(game_id)
        if game and game.status != "finished":
            return False
        else:
            return True

    def apply_result_to_profile(
        self, game_id: uuid_lib.UUID
    ) -> Tuple[Optional[Profile], Optional[Profile]]:
        game = self.data_server_controller.find_game_with_uuid(game_id)
        white_player: Optional[Profile] = None
        red_player: Optional[Profile] = None
        if game and game.status == "finished":
            white_player = game.white_player
            red_player = game.red_player
            winners: List[Profile] = []
            for winner in game.get_winner():
                if isinstance(winner, Profile):
                    winners.append(winner)

            white_player = game.white_player
            red_player = game.red_player

            """ Update the white player score """
            if white_player:
                self.data_server_controller.player_list.remove(white_player)
                if white_player in winners:
                    white_player.games_won += 1
                else:
                    white_player.games_lost += 1
                self.data_server_controller.player_list.append(white_player)

            """ Update the red player score """
            if red_player:
                self.data_server_controller.player_list.remove(red_player)
                if red_player in winners:
                    red_player.games_won += 1
                else:
                    red_player.games_lost += 1
                self.data_server_controller.player_list.append(red_player)

        return (white_player, red_player)

    def add_message_to_game(self, game_id: uuid_lib.UUID, message: Message) -> None:
        for game in self.data_server_controller.game_list:
            if game.get_uuid() == game_id:
                game.add_message(message)

    def update_profile(self, profile: Profile) -> None:
        pass

    def delete_user(self, user_id: uuid_lib.UUID) -> None:
        pass
