from __future__ import annotations

from client.data.data_controller import DataController
from common.interfaces.i_ihm_game_calls_data import I_IHMGameCallsData

from typing import Dict, List, Tuple
import uuid as uuid_lib
from common import Move, LocalGame, Player, Message, Profile
from uuid import uuid4

import pickle
from common import Profile, LocalGame, Player, User, data_structures

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from client.data.data_controller import DataController


class IhmGameCallsDataImpl(I_IHMGameCallsData):
    def __init__(self, data_controller: DataController):
        self.data_controller = data_controller

    def check_move_in_local_game(self, move: Move) -> bool:
        pass

    def leave_local_game(self) -> uuid_lib.UUID:
        pass

    def get_local_game(self) -> LocalGame:
        return self.data_controller.local_game

    def get_local_game_grid(self) -> Dict:
        return self.get_local_game().board

    def get_local_players(self) -> Tuple[Profile, Profile]:
        return Tuple(self.get_local_game().white_player,self.get_local_game().red_player)

    def get_local_game_spectators(self) -> List[Player]:
        mayo = Player("Mayo", uuid4())
        return [mayo]

    def get_local_game_messages(self) -> List[Message]:
        self.data_controller.local_game.get_chat()

    def is_local_game_finished(self) -> bool:
        return self.get_local_game().status=="finished"

    def get_local_winner(self) -> Player:
        pass

    def clear_local_game(self) -> None:
        pass

    def get_public_games(self) -> List:
        return self.data_controller.public_games

    def get_connected_players(self) -> List[Player]:
        return self.data_controller.connected_players

    def get_local_game_id(self) -> uuid_lib.UUID:
        pass

    def get_profile(self) -> Profile:
        pass

    def create_new_profile(self, user: User) -> None:
        """
        Doesnt 'create' anything.
        Saves the user object to a file.
        Does not check if the nickname is already taken.
        """
        profile_path = DataController.USER_PATH + user.nickname + ".profile"
        with open(profile_path, "wb") as profile_file:
            pickle.dump(user, profile_file)

    def create_game(self, name: str, tiles: int) -> LocalGame:
        pass

    def export_current_profile(self, destination_path: str) -> None:
        pass

    def import_profile(self, path: str) -> Profile:
        pass

    def edit_current_profile(self, profile: Profile) -> None:
        pass

    def disconnect_from_app(self) -> None:
        pass

    def disconnect_server(self) -> None:
        pass

    def connect_to_app(self, nickname: str, password: str) -> None:
        """
        Doesnt connect to the server, just retrieves the user object from a file
        """

        # retrieve the profile from the file
        user_path = (
            DataController.USER_PATH + nickname + ".user"
        )  # A uuid would have been safer
        try:
            with open(user_path, "rb") as user_file:
                user = pickle.load(user_file)

                # check if the password is correct
                if user.password == password:
                    self.data_controller.myself = user
                else:
                    raise ValueError("Wrong password")
        except FileNotFoundError:
            raise ValueError(f"User {nickname} not found.")
